## Tecnologias utilizadas no projeto
# desafio_sicredi


- Java JDK 8 - [Java JDK 8](https://www.oracle.com/br/java/technologies/javase/javase8-archive-downloads.html)

- IDE de desenvolvimento (Sugestão) - [JetBrains IntelliJ IDEA Community](https://www.jetbrains.com/pt-br/idea/download/#section=windows)

- Framework de testes automatizados de API - [RestAssured](https://rest-assured.io/)
## Getting started
- Relatório de teste - [Extent Reports](https://www.extentreports.com/)

- Relatório de teste - [Extent Reports](https://www.extentreports.com/)
To make it easy for you to get started with GitLab, here's a list of recommended next steps.
- Orquestrador de testes - [TestNG](https://testng.org/doc/)


Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!
## Abrindo o projeto (Baseado no uso do IntelliJ)


## Add your files
1. Baixar o projeto

2. Abrir IntelliJ 
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
3. Clicar em "File"
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:
4. Clicar em "Open"

5. Selecinar a pasta raiz do projeto
```
6. Clicar em "OK"
cd existing_repo
7. O projeto será carregado e as dependências serão baixadas automaticamente. Aguardar até o fim.
git remote add origin https://gitlab.com/brendsduda/desafio_sicredi.git

git branch -M main
## Serializando objetos json:
git push -uf origin main
Serialização é o processo de transformar os dados do corpo da requisição no formato json requerido (contrato)
```
Existem várias abordágens para serialização de jsons. O template oferece suporte à todas, mas como exemplo, estão implementadas duas formas:


## Integrate with your tools
No caso de siglas, manter o padrão da primeira letra, de acordo com o padrão do tipo que será nomeado, ex:


- [ ] [Set up project integrations](https://gitlab.com/brendsduda/desafio_sicredi/-/settings/integrations)
- cpfField (variável)

- preencherCPF() (método)
## Collaborate with your team


No caso de palavras com uma letra, as duas devem ser escritas juntas de acordo com o padrão do tipo que será nomeado, ex:
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)

- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- retornaSeValorEOEsperado()
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)

- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
Nomes de classes e arquivos devem terminar com o tipo de conteúdo que representam, em inglês, ex:
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


- GetTokenRequest (classe de Request)
## Test and Deploy
- LoginTests (classe de testes)

- LoginTestData.csv (planilha de dados)
Use the built-in continuous integration in GitLab.


OBS: Atenção ao plural e singular! Se a classe contém um conjunto do tipo que representa, esta deve ser escrita no plural, caso seja uma entidade, deve ser escrita no singular.
- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)

- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
Nomes dos objetos devem ser exatamente os mesmos nomes de suas classes, iniciando com letra minúscula, ex:
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)

- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- GetTokenRequest (classe) getTokenRequest (objeto)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)


Geral:
***
- Nunca utilizar acentos, caracteres especiais e “ç” para denominar pacotes, classes, métodos, variáveis, objetos e arquivos.
