package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import com.servicesdummytst.utils.GeneralUtils;
import io.restassured.http.Method;
public class PostDummyLoginRequest extends RequestRestBase {
    public PostDummyLoginRequest() {
        method = Method.POST;
        requestService = "/auth/login";
    }

    public void setJsonBody(String username, String password) {
        String jsonPath = "src/test/java/com/servicesdummytst/jsons/PostDummyLogin.json";
        jsonBody = GeneralUtils.readFileToAString(jsonPath)
                .replace("$username", username)
                .replace("$password", password);
    }
}
