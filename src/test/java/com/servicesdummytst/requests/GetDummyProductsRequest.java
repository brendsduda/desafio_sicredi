package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import io.restassured.http.Method;
public class GetDummyProductsRequest extends RequestRestBase {
    public GetDummyProductsRequest() {
        method = Method.GET;
        requestService = "/products";
    }
}
