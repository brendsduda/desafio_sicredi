package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetDummyAuthProductsRequest extends RequestRestBase {
    public GetDummyAuthProductsRequest() {
        method = Method.GET;
        headers.put("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTUsInVzZXJuYW1lIjoia21pbmNoZWxsZSIsImVtYWlsIjoia21pbmNoZWxsZUBxcS5jb20iLCJmaXJzdE5hbWUiOiJKZWFubmUiLCJsYXN0TmFtZSI6IkhhbHZvcnNvbiIsImdlbmRlciI6ImZlbWFsZSIsImltYWdlIjoiaHR0cHM6Ly9yb2JvaGFzaC5vcmcvSmVhbm5lLnBuZz9zZXQ9c2V0NCIsImlhdCI6MTcxMDQ2MzI0NCwiZXhwIjoxNzEwNDY2ODQ0fQ.VgQoMkuHb1dPlcp2rIzgwOR-fxoKaee7xwMDDVLI2ck");
        requestService = "/auth/products";
    }
}
