package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import com.servicesdummytst.utils.GeneralUtils;
import io.restassured.http.Method;
public class PostDummyProductsAddRequest extends RequestRestBase {
    public PostDummyProductsAddRequest() {
        method = Method.POST;
        requestService = "/products/add";
    }

    public void setJsonBody(String title, String description, int price, Double discountPercentage,
                            Double rating, int stock, String brand, String category, String thumbnail  ) {
        String jsonPath = "src/test/java/com/servicesdummytst/jsons/PostDummyProductsAdd.json";
        jsonBody = GeneralUtils.readFileToAString(jsonPath)
                .replace("$title", title)
                .replace("$description", description)
                .replace("$price", String.valueOf(price))
                .replace("$discountPercentage", String.valueOf(discountPercentage))
                .replace("$rating", String.valueOf(rating))
                .replace("$stock",String.valueOf(stock))
                .replace("$brand",brand)
                .replace("$category", category)
                .replace("$thumbnail", thumbnail);
    }
}
