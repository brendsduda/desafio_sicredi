package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import io.restassured.http.Method;
public class GetDummyUsersRequest extends RequestRestBase {
    public GetDummyUsersRequest() {
        method = Method.GET;
        requestService = "/users";
    }
}
