package com.servicesdummytst.requests;

import com.servicesdummytst.bases.RequestRestBase;
import io.restassured.http.Method;
public class GetDummyRequest extends RequestRestBase {
    public GetDummyRequest() {
        method = Method.GET;
        requestService = "/test";
    }
}
