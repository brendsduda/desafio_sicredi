package com.servicesdummytst.tests;


import com.servicesdummytst.bases.TestBase;
import com.servicesdummytst.requests.GetDummyAuthProductsRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

public class GetDummyAuthProductsTests extends TestBase {
    GetDummyAuthProductsRequest getDummyAuthProductsRequest;

    @Test(groups = "Principal")
    public void buscaTestComSucesso() {
        //region Arrange

        int statusCodeEsperado = HttpStatus.SC_OK;
        String titleEsperado = "iPhone 9";

        getDummyAuthProductsRequest = new GetDummyAuthProductsRequest() ;
        //endregion

        //region Act
        ValidatableResponse response = getDummyAuthProductsRequest.executeRequest();

        //endregion

        //region Assert
        response.statusCode(statusCodeEsperado);
        response.body("products.id", hasSize(30));
        response.body("products[0].title", equalTo(titleEsperado));

        //endregion
    }
}
