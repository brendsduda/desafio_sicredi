package com.servicesdummytst.tests;

import com.servicesdummytst.bases.TestBase;
import com.servicesdummytst.requests.GetDummyUsersRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.hasSize;

public class GetDummyUsersTests extends TestBase {
    GetDummyUsersRequest getDummyUsersRequest;

    @Test(groups = "Principal")
    public void buscaUsersComSucesso() {
        //region Arrange

        int statusCodeEsperado = HttpStatus.SC_OK;

        getDummyUsersRequest = new GetDummyUsersRequest();
        //endregion

        //region Act
        ValidatableResponse response = getDummyUsersRequest.executeRequest();

        //endregion

        //region Assert
        response.statusCode(statusCodeEsperado);
        response.body("users.id", hasSize(30));

        //endregion
    }
}
