package com.servicesdummytst.tests;

import com.servicesdummytst.bases.TestBase;
import com.servicesdummytst.requests.PostDummyLoginRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class PostDummyLoginTests extends TestBase {
    static PostDummyLoginRequest postDummyLoginRequest;

    @Test(groups = "Principal")
    public void obterLoginComSucesso(){
        //region Arrange
        String username = "kminchelle";
        String password = "0lelplR";

        int statusCodeEsperado = HttpStatus.SC_OK;
        int idEsperado = 15;
        String usernameEsperado = "kminchelle";
        String emailEsperado = "kminchelle@qq.com";

        postDummyLoginRequest = new PostDummyLoginRequest();
        postDummyLoginRequest.setJsonBody(username, password);
        //endregion

        //region Act
        Response response = postDummyLoginRequest.executeRequestAndGetResponse();

        //endregion

        //region Assert
        response.then().statusCode(statusCodeEsperado);
        response.then().body("id", equalTo(idEsperado),
            "username", equalTo(usernameEsperado),
                                  "email", equalTo(emailEsperado));
        //endregion
    }

    @Test(groups = "Exceção")
    public void obterLoginIncorreto(){
        //region Arrange
        String username = "kminchelleSS";
        String password = "0lelplR";

        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
        String messageEsperada = "Invalid credentials";

        postDummyLoginRequest = new PostDummyLoginRequest();
        postDummyLoginRequest.setJsonBody(username, password);

        //endregion

        //region Act
        Response response = postDummyLoginRequest.executeRequestAndGetResponse();

        //endregion

        //region Assert
        response.then().statusCode(statusCodeEsperado);
        response.then().body("message", equalTo(messageEsperada));

        //endregion
    }

    @Test(groups = "Exceção")
    public void obterSenhaIncorreto(){
        //region Arrange
        String username = "kminchelleSS";
        String password = "0lelplRAAA";

        int statusCodeEsperado = HttpStatus.SC_BAD_REQUEST;
        String messageEsperada = "Invalid credentials";

        postDummyLoginRequest = new PostDummyLoginRequest();
        postDummyLoginRequest.setJsonBody(username, password);

        //endregion

        //region Act
        Response response = postDummyLoginRequest.executeRequestAndGetResponse();

        //endregion

        //region Assert
        response.then().statusCode(statusCodeEsperado);
        response.then().body("message", equalTo(messageEsperada));

        //endregion
    }
}
