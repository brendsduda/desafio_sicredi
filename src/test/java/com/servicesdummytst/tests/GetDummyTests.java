package com.servicesdummytst.tests;

import com.servicesdummytst.bases.TestBase;
import com.servicesdummytst.requests.GetDummyRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class GetDummyTests extends TestBase {
    GetDummyRequest getDummyRequest;

    @Test(groups = "Principal")
    public void buscaTestComSucesso() {
        //region Arrange

        int statusCodeEsperado = HttpStatus.SC_OK;
        String statusEsperado = "ok";
        String methodEsperado = "GET";

        getDummyRequest = new GetDummyRequest() ;
        //endregion

        //region Act
        ValidatableResponse response = getDummyRequest.executeRequest();

        //endregion

        //region Assert
        response.statusCode(statusCodeEsperado);
        response.body("status", equalTo(statusEsperado),
      "method", equalTo(methodEsperado));
        //endregion
    }
}
