package com.servicesdummytst.tests;

import com.servicesdummytst.bases.TestBase;
import com.servicesdummytst.requests.PostDummyProductsAddRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class PostDummyProductsAddTests extends TestBase {
    PostDummyProductsAddRequest postDummyProductsAddRequest;
    public String token;

    @Test(groups = "Principal")
    public void adicionarProdutosComSucesso(){
        //region Arrange
        String title = "Perfume Oil";
        String description = "Mega Discount, Impression of A...";
        int price = 13;
        Double discountPercentage = 8.4;
        Double rating = 4.26;
        int stock = 65;
        String brand = "Impression of Acqua Di Gio";
        String category = "fragrances";
        String thumbnail = "https://i.dummyjson.com/data/products/11/thumnail.jpg";

        int statusCodeEsperado = HttpStatus.SC_OK;
        int idEsperado = 101;
        String titleEsperado = "Perfume Oil";
        int priceEsperado = 13;

        postDummyProductsAddRequest = new PostDummyProductsAddRequest();
        postDummyProductsAddRequest.setJsonBody(title, description, price, discountPercentage,
                rating, stock, brand, category, thumbnail);
        //endregion

        //region Act
       Response response = postDummyProductsAddRequest.executeRequestAndGetResponse();

       //endregion

        //region Assert
        response.then().statusCode(statusCodeEsperado);
        response.then().body("id", equalTo(idEsperado),
            "title", equalTo(titleEsperado),
                                  "price", equalTo(priceEsperado));
        //endregion
    }
}
