package com.servicesdummytst.bases;

import com.servicesdummytst.GlobalParameters;
import com.servicesdummytst.utils.ExtentReportsUtils;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;

public abstract class TestBase {

    @BeforeSuite
    public void beforSuite() {
        new GlobalParameters();
        ExtentReportsUtils.createReport();
        //Renova token antes de cada execução
        //GlobalParameters.setToken(AutenticacaoSteps.renovaToken());
    }

    @BeforeMethod
    public void beforeTest(Method method) {
        ExtentReportsUtils.addTest(method.getName(), method.getDeclaringClass().getSimpleName());
    }

    @AfterMethod
    public void afterTest(ITestResult result) {
        ExtentReportsUtils.addTestResult(result);
    }

    @AfterSuite
    public void afterSuite() {
        ExtentReportsUtils.generateReport();
    }
}